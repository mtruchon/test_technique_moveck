#include "Fibonacci.h"

namespace Fibonacci {
    std::vector<std::string> _fibonacciVector;

// adaptation en C++ de cette solution: https://stackoverflow.com/questions/494594/how-to-write-the-fibonacci-sequence
    int fibonacciNthTerm(int p_number) {
        return (pow((1 + sqrt(5)), p_number) - pow((1 - sqrt(5)), p_number)) / (pow(2, p_number) * sqrt(5));
    }

    bool fibonacci(const int &p_start, const int &p_end) {
        int n = 0;
        int current = fibonacciNthTerm(n);
        if (p_start > p_end) {
            return false;  // erreur, p_start doit être plus petit que p_end
        }
        while (current <= p_end) {
            if (p_start <= current) {
                //std::cout << current << " ";
                _fibonacciVector.push_back(std::to_string(current));
            }
            n += 1;
            current = fibonacciNthTerm(n);
        }
        //std::cout << std::endl;
        return true;
    }

    std::string showFibonacci() {
        std::string mySequence = "";
        for (int i = 0; i < _fibonacciVector.size(); i++) {
            mySequence += (_fibonacciVector[i] + " ");
        }
        return mySequence;
    }

    // cette fonction était utilisée dans ma 1re implémentation (en console)
    int askNumber(const int &p_startNumber) {
        int number;
        std::cin >> number;
        while (std::cin.fail() || number < p_startNumber) {
            std::cout << "La valeur n'est pas valide:" << std::endl;
            std::cin.clear();
            std::cin.ignore(100000, '\n');
            std::cin >> number;
        }
        std::cin.ignore();
        return number;
    }

    std::string askPath() {
        std::string filePath;
        //std::cout << "Ecrivez le chemin du fichier: " << std::endl;
        char buffer[500];
        std::cin.getline(buffer, 499);
        filePath = buffer;
        return filePath;
    }

    void saveSequence(const std::string &p_filePath) {
        std::ofstream file(p_filePath);
        if (file) {
            for (int i = 0; i < _fibonacciVector.size(); i++) {
                file << _fibonacciVector[i] << std::endl;
            }
        } else {
            //std::cout << "ERREUR: il n'est pas possible d'ouvrir le fichier." << std::endl;
            std::string newPath = askPath();
            saveSequence(newPath);
        }
    }

    void loadSequence(const std::string &p_filePath) {
        std::ifstream file(p_filePath.c_str());
        if (file) {
            std::string line;
            while (getline(file, line)) {
                _fibonacciVector.push_back(line);
            }

            for (int i = 0; i < _fibonacciVector.size(); i++) {
                //std::cout << _fibonacciVector[i] << " ";
            }
        } else {
            //std::cout << "ERREUR: il n'est pas possible d'ouvrir le fichier." << std::endl;
            std::string newPath = askPath();
            loadSequence(newPath);
        }
    }

    // cette méthode aurait été utilisée pour l'affichage de la suite (calculer le nombre de carrés nécessaires)
    int getFibonacciLength() {
        return _fibonacciVector.size();
    }
} // namespace Fibonacci