#ifndef FIB_FIBONACCI_H
#define FIB_FIBONACCI_H

#include <iostream>
#include <math.h>
#include <fstream>
#include <vector>
#include <string>

namespace Fibonacci {

    bool fibonacci(const int &p_start, const int &p_end);
    int fibonacciNthTerm(int p_number);
    std::string showFibonacci();
    int askNumber(const int &p_startNumber);
    std::string askPath();
    void saveSequence(const std::string &p_filePath);
    void loadSequence(const std::string &p_filePath);
    int getFibonacciLength();

} // namespace Fibonacci


#endif //FIB_FIBONACCI_H
